#include <stddef.h> /* size_t */

/* Partial implementation of glibc argz_* functions */

size_t __argz_count(const char *argz, size_t argz_len) 
{ 
	size_t n=0;
	for(size_t i=0; i<argz_len; i++) {
		if(argz[i]=='\0') {
			n++;
		}
	}
	return n;
}

void __argz_stringify(char *argz, size_t len, int sep)
{
	if(!len) {
		return;
	}
	for(size_t i=0; i<len-1; i++) {
		if(argz[i]=='\0') {
			argz[i]=sep;
		}
	}
}

