#include <sys/stat.h>

int isfdtype(int fd, int fdtype) {
	struct stat statbuf;
	if(fstat(fd, &statbuf)<0) return(-1);
	else return((statbuf.st_mode & S_IFMT) == (mode_t)fdtype);
}

